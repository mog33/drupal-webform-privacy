<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\HookHandler\Form\WebformAdminConfig;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\SftpWebformPrivacyAction;
use Drupal\webform\Form\AdminConfig\WebformAdminConfigElementsForm;

/**
 * Class WebformPrivacyFormAlterHookHandler.
 */
class WebformPrivacyFormAlterHookHandler extends WebformAdminConfigElementsForm {

  /**
   * Alters webform admin config form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   * @param string $formId
   *   The form id.
   *
   * @see hook_form_FORM_BASE_ID_alter()
   */
  public function formAlter(array &$form, FormStateInterface $formState, string $formId): void {
    // Instantiate our form and add our options.
    /** @var \Drupal\Core\Config\ImmutableConfig $config */
    $config = $this->config('webform_privacy.settings');
    $form['privacy'] = [
      '#type' => 'details',
      '#title' => $this->t('Privacy'),
      '#open' => FALSE,
    ];

    $this->handleWebformPrivacyMain($form['privacy'], $config);
    $this->handleWebformPrivacySftp($form['privacy'], $config);

    $form['#validate'][] = [$this, 'formValidate'];
    $form['#submit'][] = [$this, 'formSubmit'];
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public static function formValidate(array &$form, FormStateInterface $formState): void {
    $filename = $formState->getValue('filename');
    // Verify that the filename contains only lowercase letters, numbers, and
    // underscores.
    if (preg_match('@[^a-z0-9\[\]_]+@', $filename)) {
      $formState
        ->setErrorByName('filename', 'The filename can only contain lowercase letters, numbers, and underscores.');
    }
  }

  /**
   * Form submit handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function formSubmit(array &$form, FormStateInterface $formState): void {

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->configFactory();
    $config = $config_factory->getEditable('webform_privacy.settings');

    // Excluded elements.
    $excluded_elements = $this->convertIncludedToExcludedPluginIds($this->elementManager, $formState->getValue('elements_types_excluded'));

    $config->set('filename', $formState->getValue('filename'))
      ->set('enable_sftp', $formState->getValue('enable_sftp'))
      ->set('elements_types_excluded', $excluded_elements)
      ->save();
  }

  /**
   * Handle form for the webform_privacy options.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The configuration for this form.
   */
  private function handleWebformPrivacyMain(array &$form, ImmutableConfig $config): void {
    $token = $this->t('Available tokens: [sid]: Submission id, [title]: Webform title.');
    $form['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename'),
      '#description' => $this->t('Choose a filename pattern without extension for the archive generation.') . '<br>' . $token,
      '#maxlength' => 40,
      '#size' => 40,
      '#default_value' => $config->get('filename'),
      '#required' => TRUE,
    ];
    $form['allowed'] = [
      '#type' => 'details',
      '#title' => $this->t('Element types to allow'),
      '#description' => $this->t('Select non private element types to keep stored in the database.'),
      '#open' => FALSE,
    ];
    $excluded = $this->buildExcludedPlugins(
      $this->elementManager,
      $config->get('elements_types_excluded') ?? [],
    );
    $form['allowed']['elements_types_excluded'] = $excluded;
    $form['allowed']['elements_types_excluded']['#header']['title']['width'] = '25%';
    $form['allowed']['elements_types_excluded']['#header']['id']['width'] = '25%';
    $form['allowed']['elements_types_excluded']['#header']['description']['width'] = '50%';

  }

  /**
   * Handle form for the webform_privacy options.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The configuration for this form.
   *
   * @todo: move as a subform on the plugin with ConfigurableInterface.
   */
  private function handleWebformPrivacySftp(array &$form, ImmutableConfig $config): void {
    $sftpConfig = SftpWebformPrivacyAction::createConfigFromEnv();
    $form['sftp'] = [
      '#type' => 'details',
      '#title' => $this->t('SFTP connection'),
      '#open' => FALSE,
    ];
    $form['sftp']['enable_sftp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable SFTP transfer'),
      '#default_value' => $config->get('enable_sftp'),
    ];
    $form['sftp']['host'] = [
      '#type' => 'item',
      '#markup' => $this->t('Hostname: @host', ['@host' => $sftpConfig['host']]),
    ];
    $form['sftp']['port'] = [
      '#type' => 'item',
      '#markup' => $this->t('Port: @port', ['@port' => $sftpConfig['port']]),
    ];
    $form['sftp']['username'] = [
      '#type' => 'item',
      '#markup' => $this->t('Username: @username', ['@username' => $sftpConfig['username']]),
    ];
    $form['sftp']['password'] = [
      '#type' => 'item',
      '#markup' => empty($sftpConfig['password']) ? $this->t('Password not set or empty.') : $this->t('Password set in .env'),
    ];
    $form['sftp']['remotePath'] = [
      '#type' => 'item',
      '#markup' => $this->t('Path: @remotePath', ['@remotePath' => $sftpConfig['remotePath']]),
    ];
    $form['sftp']['test'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test SFTP'),
      '#submit' => [[$this, 'submitTestSftp']],
      '#limit_validation_errors' => [],
    ];
  }

  /**
   * Handle the test submit of the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function submitTestSftp(array &$form, FormStateInterface $formState): void {
    $test = SftpWebformPrivacyAction::sftpTest();
    if (TRUE === $test['status']) {
      $this->messenger()->addStatus($test['reason']);
    }
    else {
      $this->messenger()->addWarning($test['reason']);
    }
  }

}
