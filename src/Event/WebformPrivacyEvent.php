<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Event;

use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired when a filename is built.
 */
class WebformPrivacyEvent extends Event {

  /**
   * Alter the filename event.
   */
  public const ALTER_FILENAME = 'webform_privacy.alter_filename';

  /**
   * The filename generated.
   *
   * @var string
   */
  private $filename;

  /**
   * The webform entity object.
   *
   * @var \Drupal\webform\WebformInterface
   */
  private $webform;

  /**
   * The webform submission entity object.
   *
   * @var \Drupal\webform\WebformSubmissionInterface
   */
  private $submission;

  /**
   * Construct the WebformPrivacyEvent object.
   *
   * @param string $filename
   *   The filename generated.
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform entity object.
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The webform submission entity object.
   */
  public function __construct(string $filename, WebformInterface $webform, WebformSubmissionInterface $submission) {
    $this->filename = $filename;
    $this->webform = $webform;
    $this->submission = $submission;
  }

  /**
   * Gets the webform.
   *
   * @return \Drupal\webform\WebformInterface
   *   The webform entity object.
   */
  public function getWebform(): WebformInterface {
    return $this->webform;
  }

  /**
   * Gets the webform submission.
   *
   * @return \Drupal\webform\WebformSubmissionInterface
   *   The webform submission entity object.
   */
  public function getWebformSubmission(): WebformSubmissionInterface {
    return $this->submission;
  }

  /**
   * Gets the filename.
   *
   * @return string
   *   The filename generated.
   */
  public function getFilename(): string {
    return $this->filename;
  }

  /**
   * Set the filename.
   *
   * @param string $filename
   *   The filename.
   */
  public function setFilename(string $filename): void {
    $this->filename = $filename;
  }

}
