<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin\WebformPrivacyAction;

use Drupal\Core\Archiver\ArchiverException;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform_privacy\Plugin\WebformPrivacyActionInterface;
use ZipArchive;

/**
 * Manage Zip archive for Webform Privacy.
 *
 * @WebformPrivacyAction(
 *   id = "zip_archiver_webform_privacy_action",
 *   label = @Translation("Zip files"),
 *   description = @Translation("Zip printed and webform submission files."),
 *   type = "archiver",
 * )
 */
final class ZipWebformPrivacyAction implements WebformPrivacyActionInterface {

  public const ENGINE = 'zip';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static();
    $instance->fileSystem = $container->get('file_system');
    $instance->logger = $container->get('logger.factory');
    return $instance;
  }

  /**
   * Generate the archive with files.
   *
   * @param string $filename
   *   The filename to generate.
   * @param \Drupal\file\FileInterface[] $files
   *   Managed files entities.
   * @param string $scheme
   *   (Optional) The file scheme for creation, default to temporary.
   *
   * @return \Drupal\file\FileInterface
   *   The generated file.
   *
   * @throws \Drupal\Core\File\Exception\FileException
   *   If a problem occur when manipulating the archive.
   */
  public function archive(string $filename, array $files, string $scheme = 'temporary://'): FileInterface {
    $filename .= '.' . self::ENGINE;
    // Create the archive.
    $path = $this->fileSystem->realpath($scheme);
    $archive = $this->createArchive($path . '/' . $filename);

    $this->logger->get('webform_privacy')->notice('Created archive @filename', ['@filename' => $filename]);

    // Add uploaded files to the archive if any.
    if (!empty($files)) {
      foreach ($files as $file) {
        $this->addFile($file, $archive);
      }
    }

    return self::createFile($scheme . $filename);
  }

  /**
   * Create the archive.
   *
   * @param string $filename
   *   The filename of the archive with absolute path.
   *
   * @return \ZipArchive
   *   The created archive.
   *
   * @throws \Drupal\Core\Archiver\ArchiverException
   *   When archive can not be created.
   */
  private static function createArchive(string $filename): ZipArchive {
    $zip = new ZipArchive();

    if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
      throw new ArchiverException(sprintf('Cannot create %s, error: %s', $filename, $zip->getStatusString()));
    }
    return $zip;
  }

  /**
   * Create a private Drupal file entity.
   *
   * @param string $filename
   *   The filename to create.
   *
   * @return \Drupal\file\FileInterface
   *   The file managed by Drupal.
   */
  private static function createFile(string $filename): FileInterface {
    // Create the archive file as managed by Drupal.
    // Mark temporary by default in case the queue fail.
    $file = File::create([
      'uri' => $filename,
      'uid' => 1,
      'status' => 0,
    ]);
    $file->save();

    return $file;
  }

  /**
   * Add a Drupal file entity to an archive.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity to add.
   * @param \ZipArchive $archive
   *   The archive to handle.
   *
   * @throws \Drupal\Core\Archiver\ArchiverException
   *   When file can not be added to the archive.
   */
  private function addFile(FileInterface $file, ZipArchive $archive): void {
    $uri = $file->getFileUri();
    $path = $this->fileSystem->realpath($uri);
    if (!$archive->addFile($path, $file->getFilename())) {
      throw new ArchiverException(sprintf('Failed to add file %s, status: %s', $path, $archive->getStatusString()));
    }
    $this->logger->get('webform_privacy')->notice('Added file @uri to the archive.', ['@uri' => $uri]);
  }

}
