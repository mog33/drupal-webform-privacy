<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin\WebformPrivacyAction;

use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform_privacy\Plugin\WebformPrivacyActionInterface;

/**
 * Provide a purge based on Drupal temporary files for Webform Privacy.
 *
 * @WebformPrivacyAction(
 *   id = "purge_webform_privacy_action",
 *   label = @Translation("Purge files"),
 *   description = @Translation("Mark webform privacy files as temporary for deletion by Drupal."),
 *   type = "purger",
 * )
 */
final class PurgeWebformPrivacyAction implements WebformPrivacyActionInterface {

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  private $fileUsage;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static();
    $instance->fileUsage = $container->get('file.usage');
    $instance->logger = $container->get('logger.factory');
    return $instance;
  }

  /**
   * Set the files as temporary in Drupal.
   *
   * @param \Drupal\file\FileInterface[] $files
   *   Managed files entities to purge.
   */
  public function purge(array $files): void {
    // Mark all files as TEMPORARY for deletion by Drupal on cron.
    foreach ($files as $file) {
      if ($file instanceof FileInterface) {
        if ($file->isPermanent()) {
          $file->setTemporary();
          $file->save();
          // Delete usage to ensure purge by Drupal.
          $this->fileUsage->delete($file, 'webform');
          $this->logger->get('webform_privacy')->notice('Set file @fid as temporary without usage for deletion on next cron.', ['@fid' => $file->id()]);
        }
      }
    }
  }

}
