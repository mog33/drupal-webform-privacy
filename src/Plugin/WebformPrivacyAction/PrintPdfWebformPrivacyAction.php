<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin\WebformPrivacyAction;

use Drupal\entity_print\PrintEngineException;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform_privacy\Plugin\WebformPrivacyActionInterface;

/**
 * Provide a print pdf support for Webform Privacy.
 *
 * @WebformPrivacyAction(
 *   id = "entity_print_webform_privacy_action",
 *   label = @Translation("Print with Entity Print"),
 *   description = @Translation("Print a webform submission using third party entity print module."),
 *   type = "printer",
 * )
 */
final class PrintPdfWebformPrivacyAction implements WebformPrivacyActionInterface {

  public const ENGINE = 'pdf';

  /**
   * Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface definition.
   *
   * @var \Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface
   */
  private $pluginManager;

  /**
   * Drupal\entity_print\PrintBuilderInterface definition.
   *
   * @var \Drupal\entity_print\PrintBuilderInterface
   */
  private $printBuilder;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static();
    $instance->pluginManager = $container->get('plugin.manager.entity_print.print_engine');
    $instance->printBuilder = $container->get('entity_print.print_builder');
    $instance->renderer = $container->get('renderer');
    $instance->logger = $container->get('logger.factory');
    return $instance;
  }

  /**
   * Generate the printable version of the submission.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The webform submission entity before being cleaned and saved.
   * @param string $filename
   *   The filename to generate.
   *
   * @return \Drupal\file\FileInterface
   *   The generated file entity.
   */
  public function print(WebformSubmissionInterface $submission, string $filename): FileInterface {
    // Generate the unmanaged by Drupal file with entity_print.
    $uri = $this->printEntityPrint($submission, $filename);

    $this->logger->get('webform_privacy')->notice('Generated print submission @uri', ['@uri' => $uri]);

    // Set the file to be managed by Drupal for easier management and cleanup.
    // Mark temporary by default in case the queue fail.
    $file = File::create([
      'uri' => $uri,
      'uid' => 1,
      'status' => 0,
    ]);
    $file->save();

    return $file;
  }

  /**
   * Use entity_print pdf engine to generate the pdf.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The webform submission entity before being cleaned and saved.
   * @param string $filename
   *   The filename.
   *
   * @return string
   *   The uri of the file created.
   *
   * @throws \Drupal\entity_print\PrintEngineException
   *   When file print creation fail.
   */
  private function printEntityPrint(WebformSubmissionInterface $submission, string $filename): string {
    $filename .= '.' . self::ENGINE;

    // Use entity_print to generate the print with any engine configured.
    $printEngine = $this->pluginManager->createSelectedInstance(self::ENGINE);

    // Add the Submission information as it's not part of the default entity
    // display.
    $printEngine->addPage($this->renderSubmissionInformation($submission));

    // Generate the unmanaged by Drupal file with entity_print.
    $file = $this->printBuilder->savePrintable([$submission], $printEngine, 'private', $filename, FALSE);
    if (!$file) {
      throw new PrintEngineException(sprintf('Fail to create the print file of submission %d.', $submission->id()));
    }
    return $file;
  }

  /**
   * Render the submission information part.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The webform submission entity loaded and cleaned.
   *
   * @return string
   *   The HTML content of the submission information.
   */
  private function renderSubmissionInformation(WebformSubmissionInterface $submission): string {
    $render = [
      '#theme' => 'webform_privacy_submission_information',
      '#webform_submission' => $submission,
    ];
    return $this->renderer->renderPlain($render)->__toString();
  }

}
