<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin\WebformPrivacyAction;

use Drupal\Core\FileTransfer\FileTransferException;
use Drupal\file\FileInterface;
use phpseclib\Net\SFTP;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform_privacy\Plugin\WebformPrivacyActionInterface;

/**
 * Provide SFTP transfer of files for Webform Privacy.
 *
 * @WebformPrivacyAction(
 *   id = "sftp_webform_privacy_action",
 *   label = @Translation("Send by SFTP"),
 *   description = @Translation("Send webform submission and files on a remote SFTP."),
 *   type = "sender",
 * )
 */
final class SftpWebformPrivacyAction implements WebformPrivacyActionInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * SFTP connection configuration.
   *
   * @var array
   */
  private $config;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static();
    $instance->fileSystem = $container->get('file_system');
    $instance->logger = $container->get('logger.factory');
    $instance->config = self::createConfigFromEnv();
    return $instance;
  }

  /**
   * Send a file through SFTP.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity to send.
   *
   * @throws \Drupal\Core\FileTransfer\FileTransferException
   *   When sftp login or put file fail.
   */
  public function send(FileInterface $file): void {
    $sftp = new SFTP($this->config['host'], $this->config['port']);
    // If config is not valid, this will fail.
    if (!$sftp->login($this->config['username'], $this->config['password'])) {
      throw new FileTransferException(sprintf('SFTP login failed to %s@%s:%s', $this->config['username'], $this->config['host'], $this->config['port']));
    }
    else {
      $local = $this->fileSystem->realpath($file->getFileUri());
      $remote = $this->config['remotePath'] . '/' . $file->getFilename();
      if (!$sftp->put($remote, $local, SFTP::SOURCE_LOCAL_FILE)) {
        throw new FileTransferException(sprintf('SFTP failed to send %s to %s, please check your sftp permissions', $local, $remote));
      }
      // Log to keep track of the process.
      $destination = $this->config['username'] . '@' . $this->config['host'] . ':' . $this->config['port'] . $this->config['remotePath'];
      $this->logger->get('webform_privacy')->notice('Archive @uri sent to SFTP @destination', ['@uri' => $file->getFileUri(), '@destination' => $destination]);
    }
  }

  /**
   * Return the SFTP config from .env file.
   *
   * @return array
   *   The SFTP config.
   */
  public static function createConfigFromEnv(): array {
    return [
      'host' => getenv('WEBFORM_PRIVACY_SFTP_HOST'),
      'port' => getenv('WEBFORM_PRIVACY_SFTP_PORT') ? getenv('WEBFORM_PRIVACY_SFTP_PORT') : 22,
      'remotePath' => getenv('WEBFORM_PRIVACY_SFTP_PATH') ? getenv('WEBFORM_PRIVACY_SFTP_PATH') : '/',
      'username' => getenv('WEBFORM_PRIVACY_SFTP_USERNAME'),
      'password' => getenv('WEBFORM_PRIVACY_SFTP_PASSWORD'),
    ];
  }

  /**
   * Test the SFTP connection.
   *
   * @return array
   *   Result array with key 'status' as boolean and 'reason' for a message.
   */
  public static function sftpTest(): array {
    $config = self::createConfigFromEnv();
    $context = [
      '@username' => $config['username'],
      '@host' => $config['host'],
      '@port' => $config['port'],
    ];

    $sftp = new SFTP($config['host'], $config['port']);
    if (!$sftp->login($config['username'], $config['password'])) {
      return [
        'status' => FALSE,
        'reason' => t('SFTP login failed @host:@port with user @username, please check your login details and the server ssh configuration.', $context),
      ];
    }
    else {
      $local = tempnam('', 'SftpTest');
      $remote = $config['remotePath'] . '/test';

      if ($sftp->put($remote, $local, SFTP::SOURCE_LOCAL_FILE)) {
        $sftp->delete($remote);
        return [
          'status' => TRUE,
          'reason' => t('SFTP test succeeded!'),
        ];
      }
      else {
        return [
          'status' => FALSE,
          'reason' => t('SFTP failed to send file, please check permissions on the remote SFTP.'),
        ];
      }
    }
  }

}
