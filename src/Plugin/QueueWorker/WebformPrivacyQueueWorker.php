<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin\QueueWorker;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Site\Settings;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\webform_privacy\Event\WebformPrivacyEvent;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\PrintPdfWebformPrivacyAction;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\PurgeWebformPrivacyAction;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\SftpWebformPrivacyAction;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\ZipWebformPrivacyAction;
use Drupal\webform_privacy\Plugin\WebformPrivacyActionManager;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Executes interface translation queue tasks.
 *
 * @QueueWorker(
 *   id = "webform_privacy",
 *   title = @Translation("Webform privacy submissions to process."),
 *   cron = {"time" = 15}
 * )
 */
final class WebformPrivacyQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The plugin manager for our available actions.
   *
   * @var \Drupal\webform_privacy\Plugin\WebformPrivacyActionManager
   */
  protected $pluginManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * An event dispatcher instance to use.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a new WebformPrivacyQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\webform_privacy\Plugin\WebformPrivacyActionManager $plugin_manager
   *   The plugin manager for our available actions.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   An event dispatcher instance to use.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    WebformPrivacyActionManager $plugin_manager,
    ConfigFactoryInterface $config_factory,
    ContainerAwareEventDispatcher $event_dispatcher,
    EntityRepositoryInterface $entity_repository
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->pluginManager = $plugin_manager;
    $this->configFactory = $config_factory;
    $this->eventDispatcher = $event_dispatcher;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.webform_privacy_action'),
      $container->get('config.factory'),
      $container->get('event_dispatcher'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    if (!Settings::get('file_private_path')) {
      throw new \Exception('Drupal private filesystem is not properly set, please fix it and launch cron.');
    }

    /** @var \Drupal\webform\WebformSubmissionInterface $submission */
    $submission = $data['entity'];
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $data['entity_parent'];

    if (!$submission instanceof WebformSubmissionInterface && !$webform instanceof WebformInterface) {
      throw new \Exception('The queue item contain an invalid submission entity.');
    }

    $files = [];
    if (!empty($data['files'])) {
      /** @var \Drupal\file\FileInterface[] $files */
      $files = File::loadMultiple($data['files']);
    }

    // Load the config and prepare the filename.
    $config = $this->configFactory->get('webform_privacy.settings');

    // We need to reload the submission to access all metadata.
    $loadedSubmission = $this->entityRepository->loadEntityByUuid('webform_submission', $submission->uuid());
    $filename = $this->buildFilename($config->get('filename'), $webform, $loadedSubmission);

    // Start the action workflow by printing the submission.
    $print = $this->createPrint($submission, $filename);
    $files[$print->id()] = $print;

    // Create the archive with print and any files uploaded to the submission.
    $archive = $this->createArchive($filename, $files);
    $files[$archive->id()] = $archive;

    // Send the archive and purge the files.
    if ($config->get('enable_sftp')) {
      $this->sendArchive($archive);
      $this->purgeFiles($files);
    }
  }

  /**
   * Print the submission, need to original submission with the data.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The original submission.
   * @param string $filename
   *   The filename to use.
   *
   * @return \Drupal\file\FileInterface
   *   File entity of printed submission.
   */
  private function createPrint(WebformSubmissionInterface $submission, string $filename): FileInterface {
    /** @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\PrintPdfWebformPrivacyAction $plugin */
    $plugin = $this->pluginManager->getActionInstance('printer');
    if (!$plugin instanceof PrintPdfWebformPrivacyAction) {
      throw new \LogicException('Wrong plugin type used to instantiate the printer plugin.');
    }
    return $plugin->print($submission, $filename);
  }

  /**
   * Archive the files.
   *
   * @param string $filename
   *   The filename to use.
   * @param \Drupal\file\FileInterface[] $files
   *   Managed files entities.
   *
   * @return \Drupal\file\FileInterface
   *   The archive created.
   */
  private function createArchive(string $filename, array $files): FileInterface {
    /** @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\ZipWebformPrivacyAction $plugin */
    $plugin = $this->pluginManager->getActionInstance('archiver');
    if (!$plugin instanceof ZipWebformPrivacyAction) {
      throw new \LogicException('Wrong plugin type used to instantiate the archiver plugin.');
    }
    return $plugin->archive($filename, $files);
  }

  /**
   * Send the archive.
   *
   * @param \Drupal\file\FileInterface $archive
   *   The managed archive to send.
   */
  private function sendArchive(FileInterface $archive): void {
    /** @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\SftpWebformPrivacyAction $plugin */
    $plugin = $this->pluginManager->getActionInstance('sender');
    if (!$plugin instanceof SftpWebformPrivacyAction) {
      throw new \LogicException('Wrong plugin type used to instantiate the sender plugin.');
    }
    $plugin->send($archive);
  }

  /**
   * Purge the files.
   *
   * @param \Drupal\file\FileInterface[] $files
   *   Managed files entities.
   */
  private function purgeFiles(array $files): void {
    /** @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\PurgeWebformPrivacyAction $plugin */
    $plugin = $this->pluginManager->getActionInstance('purger');
    if (!$plugin instanceof PurgeWebformPrivacyAction) {
      throw new \LogicException('Wrong plugin type used to instantiate the purger plugin.');
    }
    $plugin->purge($files);
  }

  /**
   * Build the filename if needed based on some tokens.
   *
   * @param string $filename
   *   The filename to build.
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform entity for this submission.
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The submission entity.
   *
   * @return string
   *   Filename with replacements.
   */
  private function buildFilename(string $filename, WebformInterface $webform, WebformSubmissionInterface $submission): string {
    // Replace default token values.
    $replacement = [
      '[sid]' => $submission->id(),
      '[title]' => $webform->label(),
    ];
    $filename = strtr($filename, $replacement);

    // Instantiate our event to let other modules alter this filename.
    $event = new WebformPrivacyEvent($filename, $webform, $submission);
    // Get the event_dispatcher service and dispatch the event.
    $this->eventDispatcher->dispatch(WebformPrivacyEvent::ALTER_FILENAME, $event);
    $filename = $event->getFilename();

    return $filename;
  }

}
