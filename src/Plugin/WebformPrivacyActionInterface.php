<?php

namespace Drupal\webform_privacy\Plugin;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for Webform privacy action plugins.
 */
interface WebformPrivacyActionInterface extends ContainerFactoryPluginInterface {
}
