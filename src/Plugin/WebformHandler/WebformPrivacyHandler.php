<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission privacy handler.
 *
 * @WebformHandler(
 *   id = "privacy",
 *   label = @Translation("Privacy"),
 *   category = @Translation("Internal"),
 *   description = @Translation("Ensure submissions are not kept in database. Must be set as the last handler."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
final class WebformPrivacyHandler extends WebformHandlerBase {

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    WebformSubmissionConditionsValidatorInterface $conditions_validator,
    QueueFactory $queue_factory
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $logger_factory,
      $config_factory,
      $entity_type_manager,
      $conditions_validator
    );
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(WebformSubmissionInterface $webformSubmission): void {
    $webform = $webformSubmission->getWebform();
    $data = $webformSubmission->getData();

    // Initiate and prepare the Queue.
    $queue = $this->queueFactory->get('webform_privacy');
    $queue->createQueue();

    // Initiate our queue item with the webform entity.
    $queueItem = [
      'entity_parent' => $webform,
    ];

    // Get the uploaded files in the form as entities.
    $queueItem['files'] = $this->filterFiles($webform, $data);

    // Set submission entity clone before cleanup so we keep the submission
    // values.
    $queueItem['entity'] = $webformSubmission;

    // Add this submission as an item to the Queue for cron processing
    // by the WebformPrivacyQueueWorker.
    $qid = $queue->createItem($queueItem);

    $this->getLogger('webform_privacy')->notice('Submission processing planned with Queue @qid.', ['@qid' => $qid]);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webformSubmission, $update = TRUE): void {
    // If this submission is an update, we don't need to do anything.
    if ('update' === $update) {
      $this->getLogger('webform_privacy')->notice('Skip submission %sid: update.', ['%sid' => $webformSubmission->id()]);
      return;
    }
    // If this form is set to not save any data, we don't need to do anything.
    if ($webformSubmission->getWebform()->getSetting('results_disabled')) {
      $this->getLogger('webform_privacy')->notice('Skip submission %sid: results disabled.', ['%sid' => $webformSubmission->id()]);
      return;
    }
    // If this form is not completed, we don't need to do anything.
    if ($webformSubmission->getState() !== WebformSubmissionInterface::STATE_COMPLETED) {
      $this->getLogger('webform_privacy')->notice('Skip submission %sid: not completed.', ['%sid' => $webformSubmission->id()]);
      return;
    }

    // Initiate the data and elements to process.
    $data = $webformSubmission->getData();
    $elements = $webformSubmission->getWebform()->getElementsDecodedAndFlattened();

    // Get configuration for the module, mostly elements to handle.
    $config = $this->configFactory->get('webform_privacy.settings');

    // Clean the submission by removing values on private data.
    $dataPrivatized = $this->getDataPrivatized($config, $elements, $data);

    // Set the new data.
    $webformSubmission->setData($dataPrivatized);
    // Resave the webform submission without trigger any hooks or handlers.
    $webformSubmission->resave();

    $this->getLogger('webform_privacy')->notice('Submission %sid obfuscated.', ['%sid' => $webformSubmission->id()]);
  }

  /**
   * Load the submitted files as entities.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform parent of the submission.
   * @param array $data
   *   Submission data array as form element key => value.
   *
   * @return array
   *   The files loaded.
   */
  private function filterFiles(WebformInterface $webform, array $data): array {
    $fileKeys = $webform->getElementsManagedFiles();
    $files = [];
    foreach ($data as $key => $value) {
      if (isset($fileKeys[$key]) && !empty($value)) {
        $files[] = $value;
      }
    }
    return $files;
  }

  /**
   * Privatize the submission by removing the values.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The module configuration.
   * @param array $elements
   *   The webform elements decoded as an associative array.
   * @param array $data
   *   The submission data.
   *
   * @return array
   *   The privatized data.
   */
  private function getDataPrivatized(ImmutableConfig $config, array $elements, array $data): array {
    // Clean the data based on configuration (empty the value).
    $excludedElements = $config->get('elements_types_excluded') ?? [];
    $dataPrivatized = [];

    if (!empty($excludedElements)) {
      foreach ($elements as $key => $element) {
        // Keep a list of excluded elements with an empty value for replacement.
        if (\in_array($element['#type'], $excludedElements, TRUE)) {
          $dataPrivatized[$key] = '';
        }
      }
    }
    // Cleanup private data and replace with empty value.
    return \array_merge($data, $dataPrivatized);
  }

}
