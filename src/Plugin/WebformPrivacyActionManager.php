<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Provides the Webform privacy action plugin manager.
 */
class WebformPrivacyActionManager extends DefaultPluginManager {

  /**
   * Constructs a new WebformPrivacyActionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/WebformPrivacyAction', $namespaces, $module_handler, 'Drupal\webform_privacy\Plugin\WebformPrivacyActionInterface', 'Drupal\webform_privacy\Annotation\WebformPrivacyAction');

    $this->alterInfo('webform_privacy_action_info');
    $this->setCacheBackend($cache_backend, 'webform_privacy_action_plugins');
  }

  /**
   * Instantiate an action by type.
   *
   * @param string $type
   *   The action plugin type.
   *
   * @return \Drupal\webform_privacy\Plugin\WebformPrivacyActionInterface
   *   The action plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   When there is no plugin of this type.
   */
  public function getActionInstance(string $type): WebformPrivacyActionInterface {
    $definitions = $this->getDefinitions();
    foreach ($definitions as $plugin) {
      if ($type === $plugin['type']) {
        return $this->createInstance($plugin['id']);
      }
    }
    throw new PluginNotFoundException(sprintf('Can not find any action for %s', $type));
  }

}
