<?php

declare(strict_types=1);

namespace Drupal\webform_privacy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Webform privacy action item annotation object.
 *
 * @see \Drupal\webform_privacy\Plugin\WebformPrivacyActionManager
 * @see plugin_api
 *
 * @Annotation
 */
class WebformPrivacyAction extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the action plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The plugin type.
   *
   * @var string
   */
  public $type;

}
