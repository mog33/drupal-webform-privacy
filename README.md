CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirments
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Provide privacy to webform submission with a handler by erasing sensitive data
from the database and sending the submission and files on a remote sftp.

REQUIREMENTS
-------------

- [Webform module](https://www.drupal.org/project/webform)
- [Entity print module](https://www.drupal.org/project/entity_print)

INSTALLATION
-------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Configure from webform /admin/structure/webform/config/handlers
