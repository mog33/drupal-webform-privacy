<?php

namespace Drupal\Tests\webform_privacy\Functional;

use Drupal\Core\Serialization\Yaml;

/**
 * Test module config form.
 *
 * @group webform_privacy
 */
class WebformPrivacyConfigTest extends WebformPrivacyBrowserTestBase {

  /**
   * Test the config form.
   */
  public function testConfigForm() {

    // Access config page.
    $this->drupalGet('/admin/structure/webform/config/handlers');
    $this->assertSession()->statusCodeEquals(200);

    // Test our handler is in the list and enabled.
    $this->assertSession()->checkboxChecked('edit-excluded-handlers-privacy');

    // Test the form elements exist and have defaults.
    $config = $this->config('webform_privacy.settings');

    $this->assertSession()->fieldValueEquals(
      'edit-filename',
      $config->get('filename')
    );
    $this->assertSession()->checkboxChecked('edit-enable-sftp');

    // Test filename change submission.
    $newValidFilename = 'test_[title]_[sid]';
    $this->drupalPostForm(NULL, [
      'filename' => $newValidFilename,
      'enable_sftp' => 0,
    ], 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    $this->assertSession()->fieldValueEquals(
      'filename',
      $newValidFilename
    );

    $this->assertSession()->checkboxNotChecked('edit-enable-sftp');

    // Test invalid filename change.
    $newInvalidFilename = 'test something [title] ! .test';
    $this->drupalPostForm(NULL, [
      'filename' => $newInvalidFilename,
    ], 'Save configuration');

    $this->assertSession()->pageTextContains('The filename can only contain lowercase letters, numbers, and underscores.');
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() {
    // Reset config to default.
    $config = $this->config('webform_privacy.settings');
    $file = \file_get_contents(__DIR__ . '/../../../config/install/webform_privacy.settings.yml');
    $config->setData(Yaml::decode($file));

    parent::tearDown();
  }

}
