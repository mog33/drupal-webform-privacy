<?php

namespace Drupal\Tests\webform_privacy\Functional;

/**
 * Test the module privacy handler.
 *
 * @group webform_privacy
 */
class WebformPrivacyHandlerTest extends WebformPrivacyBrowserTestBase {

  /**
   * Test to add /delete the handler to a webform.
   */
  public function testHandler() {
    // Add our handler to the webform.
    $this->drupalGet('admin/structure/webform/manage/' . $this->formId . '/handlers/add/privacy');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('The webform handler was successfully added.');

    // Delete our handler.
    $this->drupalGet('admin/structure/webform/manage/' . $this->formId . '/handlers/privacy/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains('has been deleted.');
  }

}
