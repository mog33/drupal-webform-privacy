<?php

namespace Drupal\Tests\webform_privacy\Functional;

/**
 * Test the module privacy.
 *
 * @group webform_privacy
 */
class WebformPrivacySubmissionTest extends WebformPrivacyBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->handler = $this->instantiateHandler();
  }

  /**
   * Test a submission privacy.
   */
  public function testSubmission() {

    // Enable our handler on the webform.
    $this->webform->addWebformHandler($this->handler);

    // Visit the form and fill as a visitor.
    $this->drupalLogout();
    $this->drupalGet('webform/' . $this->formId);
    $this->assertSession()->statusCodeEquals(200);

    // Populate the webform.
    $data = $this->getData();

    $this->drupalPostForm(NULL, $data['edit'], 'Submit');
    $this->assertSession()->statusCodeEquals(200);

    // Access the submission and check values are cleaned.
    $this->drupalLogin($this->adminUser);
    $sid = $this->getLastSubmissionId($this->webform);
    $this->drupalGet('/admin/structure/webform/manage/' . $this->formId . '/submission/' . $sid);
    $this->assertSession()->statusCodeEquals(200);

    // Ensure values are cleaned.
    foreach ($data['expected'] as $id => $text) {
      $element = $this->cssSelect('#test--' . $id);
      if (isset($element[0])) {
        $this->assertSame($text, $element[0]->getText());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() {
    // Remove handler.
    $this->webform->deleteWebformHandler($this->handler);
    // Remove submission.
    $this->purgeSubmissions();

    parent::tearDown();
  }

}
