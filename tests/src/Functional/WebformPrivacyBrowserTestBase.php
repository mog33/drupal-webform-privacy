<?php

namespace Drupal\Tests\webform_privacy\Functional;

use Drupal\Core\Serialization\Yaml;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\Traits\Core\CronRunTrait;
use Drupal\Tests\webform_privacy\Traits\WebformPrivacyTestTrait;
use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\webform\WebformInterface;

/**
 * Test the module privacy.
 *
 * @group webform_privacy
 */
class WebformPrivacyBrowserTestBase extends WebformBrowserTestBase {

  use CronRunTrait;
  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }
  use WebformPrivacyTestTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'file',
    'webform',
    'webform_privacy',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * File usage manager.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * An user with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Id of the webform to use.
   *
   * @var string
   */
  protected $formId = 'privacy';

  /**
   * The webform used.
   *
   * @var \Drupal\webform\WebformInterface
   */
  protected $webform;

  /**
   * The webform privacy handler.
   *
   * @var \Drupal\webform\Plugin\WebformHandlerInterface
   */
  protected $handler;

  /**
   * Id of the queue to use.
   *
   * @var string
   */
  protected $queueName = 'webform_privacy';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create and log in an administrative user.
    $this->adminUser = $this
      ->drupalCreateUser([
        'administer webform',
      ]);
    $this->drupalLogin($this->adminUser);

    $this->fileUsage = $this->container->get('file.usage');

    $this->webform = $this->createPrivacyWebform();
  }

  /**
   * Create a webform.
   */
  public function createPrivacyWebform(): WebformInterface {
    // Check create webform with file upload elements.
    $values = ['id' => $this->formId];
    $file = \file_get_contents(__DIR__ . '/../../fixtures/test_form_nojs.yml');
    $elements = Yaml::decode($file);

    $webform = $this->createWebform($values, $elements);
    $this->assertNotNull($webform);

    return $webform;
  }

  /**
   * Get data to fill and expected for the submission.
   */
  public function getData(): array {
    $data = WebformPrivacyTestTrait::getData();
    $file = current($this->drupalGetTestFiles('text'));
    $data['files[managed_file]'] = $this->container->get('file_system')->realpath($file->uri);
    return $data;
  }

  /**
   * Get the last item queue.
   */
  public function getQueueLastItemData() {
    $queue = \Drupal::queue($this->queueName, TRUE);
    $this->assertEqual($queue->numberOfItems(), 1, 'Queue is empty');
    $this->assertSame(1, $queue->numberOfItems(), 'Queue contain one item.');
    $item = $queue->claimItem();
    return $item;
  }

}
