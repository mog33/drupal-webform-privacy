<?php

namespace Drupal\Tests\webform_privacy\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\PrintPdfWebformPrivacyAction;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface;
use Drupal\entity_print\PrintBuilderInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Class PrintWebformPrivacyActionTest.
 *
 * @group webform_privacy
 */
class PrintWebformPrivacyActionTest extends UnitTestCase {

  /**
   * Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface definition.
   *
   * @var \Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Drupal\entity_print\PrintBuilderInterface definition.
   *
   * @var \Drupal\entity_print\PrintBuilderInterface
   */
  protected $printBuilder;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $renderer;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $logger;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->pluginManager = $this->createMock(EntityPrintPluginManagerInterface::class);
    $this->printBuilder = $this->createMock(PrintBuilderInterface::class);
    $this->renderer = $this->createMock(RendererInterface::class);
    $this->logger = $this->createMock(LoggerChannelInterface::class);
  }

  /**
   * Test the create().
   */
  public function testCreate(): void {
    $container = $this->createMock(ContainerInterface::class);
    $map = [
      $this->pluginManager,
      $this->printBuilder,
      $this->renderer,
      $this->logger,
    ];
    $container->expects($this->exactly(4))
      ->method('get')
      ->will($this->returnValueMap($map));

    $this->assertInstanceOf(
      PrintPdfWebformPrivacyAction::class,
      PrintPdfWebformPrivacyAction::create(
        $container,
        [],
        'plugin_id',
        ['provider' => 'webform_privacy']
      )
    );
  }

}
