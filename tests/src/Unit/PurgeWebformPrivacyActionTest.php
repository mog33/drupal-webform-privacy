<?php

namespace Drupal\Tests\webform_privacy\Unit;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\PurgeWebformPrivacyAction;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Class PurgeWebformPrivacyActionTest.
 *
 * @group webform_privacy
 */
class PurgeWebformPrivacyActionTest extends UnitTestCase {

  /**
   * The mocked filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  private $fileSystem;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $logger;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fileSystem = $this->createMock(FileSystemInterface::class);
    $this->logger = $this->createMock(LoggerChannelInterface::class);
  }

  /**
   * Test the create().
   */
  public function testCreate(): void {
    $container = $this->createMock(ContainerInterface::class);
    $map = [
      $this->fileSystem,
      $this->logger,
    ];
    $container->expects($this->exactly(2))
      ->method('get')
      ->will($this->returnValueMap($map));

    $this->assertInstanceOf(
      PurgeWebformPrivacyAction::class,
      PurgeWebformPrivacyAction::create(
        $container,
        [],
        'plugin_id',
        ['provider' => 'webform_privacy']
      )
    );
  }

}
