<?php

namespace Drupal\Tests\webform_privacy\Traits;

use Drupal\webform\Plugin\WebformHandlerInterface;

/**
 * Provides data for tests.
 */
trait WebformPrivacyTestTrait {

  /**
   * Create handler.
   */
  public static function instantiateHandler(): WebformHandlerInterface {
    $handler = \Drupal::service('plugin.manager.webform.handler')->createInstance('privacy');
    $handler->setHandlerId('privacy')
      ->setLabel('privacy')
      ->setStatus(1)
      ->setWeight(99);
    return $handler;
  }

  /**
   * Form submission data and result expected.
   *
   * @return array
   *   The data to edit and expected for test.
   */
  public static function getData(): array {
    $edit = [
      'checkbox' => 1,
      'textarea' => 'This is a test.',
      'textfield' => 'This is a test.',
      'email' => 'name@test.com',
      'number' => 5,
      'search' => 'Test',
      'tel' => '+33 5 55 44 33',
      'url' => 'http://test.com',
      'checkboxes[one]' => 'one',
      'checkboxes[two]' => 'two',
      'radios' => 'two',
      'select' => 'one',
      'tableselect[one]' => 'One',
    ];

    $expected = [
      'textarea' => 'Textarea {Empty}',
      'textfield' => 'Textfield {Empty}',
      'email' => 'Email {Empty}',
      'number' => 'Number {Empty}',
      'search' => 'Search {Empty}',
      'tel' => 'Telephone {Empty}',
      'url' => 'URL {Empty}',
      'managed_file_File' => 'File {Empty}',
      'checkbox' => 'Checkbox Yes',
      'hidden' => 'Hidden I-am-hidden',
      'checkboxes' => 'Checkboxes One, Two',
      'radios' => 'Radios Two',
      'select' => 'Select One',
      'tableselect' => 'Tableselect one',
    ];
    return ['edit' => $edit, 'expected' => $expected];
  }

  /**
   * Form submission data and result expected with js elements.
   *
   * @return array
   *   The data to edit and expected for test.
   */
  public static function getDataJs(): array {

    $edit = [
      'webform_autocomplete' => 'test',
      'webform_mapping[one]' => 'four',
      'webform_mapping[two]' => 'five',
      'webform_mapping[three]' => 'six',
      'webform_checkboxes_other[checkboxes][two]' => 'two',
      'webform_checkboxes_other[checkboxes][_other_]' => '_other_',
      'webform_likert[q1]' => 1,
      'webform_likert[q2]' => 2,
      'webform_likert[q3]' => 3,
      'webform_radios_other[radios]' => 'three',
      'webform_select_other[select]' => 'three',
      'tableselect_sort[one][checkbox]' => 'one',
    ];

    $expected = [
      'webform_autocomplete' => 'Autocomplete {Empty}',
      'webform_mapping' => 'Mapping {Empty}',
      'webform_audio_file_Audio' => 'Audio {Empty}',
      'webform_document_file_Document' => 'Document {Empty}',
      'webform_image_file_Image' => 'Image {Empty}',
      'webform_video_file_Video' => 'Video {Empty}',
      'webform_checkboxes_other' => 'Checkboxes Two, webform_checkboxes_other[other]',
      'webform_likert' => 'something',
      'webform_radios_other' => 'Radios other Three',
      'webform_select_other' => 'Select other Three',
      'tableselect_sort' => 'Tableselect sort one',
    ];

    $data = ['edit' => $edit, 'expected' => $expected];
    return array_merge_recursive(self::getData(), $data);
  }

}
