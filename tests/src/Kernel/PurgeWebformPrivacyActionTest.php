<?php

namespace Drupal\Tests\webform_privacy\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\PurgeWebformPrivacyAction;
use Drupal\file\Entity\File;

/**
 * Class PurgeWebformPrivacyActionTest.
 *
 * @group webform_privacy
 */
class PurgeWebformPrivacyActionTest extends KernelTestBase {

  /**
   * The purge action to test.
   *
   * @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\PurgeWebformPrivacyAction
   */
  protected $action;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['system', 'file', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['system']);
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);

    $this->action = PurgeWebformPrivacyAction::create(
      $this->container,
      [],
      'plugin_id',
      'plugin_definition'
    );
  }

  /**
   * Test the method purge().
   */
  public function testPurge() {
    $file_usage = $this->container->get('file.usage');

    // Create a file.
    $filename = 'test.txt';
    /** @var \Drupal\file\FileInterface $file */
    $file = File::create([
      'uri' => 'public://' . $filename,
    ]);
    $file->setPermanent();
    // Create the file itself.
    file_put_contents($file->getFileUri(), $this->randomString());
    $file->save();

    $this->action->purge([$file]);
    $this->assertSame($file->isTemporary(), TRUE);
    $this->assertSame($file_usage->listUsage($file), []);
  }

}
