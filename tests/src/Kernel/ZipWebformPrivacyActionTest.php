<?php

namespace Drupal\Tests\webform_privacy\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\ZipWebformPrivacyAction;

/**
 * Class ZipWebformPrivacyActionTest.
 *
 * @group webform_privacy
 */
class ZipWebformPrivacyActionTest extends KernelTestBase {

  /**
   * The zip action to test.
   *
   * @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\ZipWebformPrivacyAction
   */
  protected $action;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['system', 'file', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['system']);
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);

    $this->action = ZipWebformPrivacyAction::create(
      $this->container,
      [],
      'plugin_id',
      'plugin_definition'
    );
  }

  /**
   * Test the method archive().
   */
  public function testArchive() {
    // Create an archive without files.
    $archive = $this->action->archive('test_1', []);
    $this->assertSame($archive->getFilename(), 'test_1.' . $this->action::ENGINE);
    $this->assertSame($archive->isTemporary(), TRUE);

    // @todo: Create an archive with one file simulated as uploaded from
    // webform.
    // Seems hard to unit test ZipArchive, see
    // https://github.com/bovigo/vfsStream/wiki/Known-Issues
  }

}
