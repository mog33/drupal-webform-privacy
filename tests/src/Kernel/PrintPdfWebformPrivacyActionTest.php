<?php

namespace Drupal\Tests\webform_privacy\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\webform_privacy\Plugin\WebformPrivacyAction\PrintPdfWebformPrivacyAction;

/**
 * Class PrintPdfWebformPrivacyActionTest.
 *
 * @requires module entity_print
 *
 * @group webform_privacy
 */
class PrintPdfWebformPrivacyActionTest extends KernelTestBase {

  /**
   * The print action to test.
   *
   * @var \Drupal\webform_privacy\Plugin\WebformPrivacyAction\ZipWebformPrivacyAction
   */
  protected $action;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Webform submission.
   *
   * @var \Drupal\webform\WebformSubmissionInterface
   */
  protected $webformSubmission;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['entity_print', 'entity_print_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->renderer = $this->container->get('renderer');
    $this->webformSubmission = $this->getMockBuilder('Drupal\webform\WebformSubmissionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $config = $this->container->get('config.factory')->getEditable('entity_print.settings');
    $config
      ->set('print_engines.pdf_engine', 'testprintengine')
      ->save();

    $this->action = PrintPdfWebformPrivacyAction::create(
      $this->container,
      [],
      'plugin_id',
      'plugin_definition'
    );
  }

  /**
   * Test the method print().
   */
  public function testPrint() {
    // @todo: see how to deal with 'Rendering not yet supported'
    $this->markTestIncomplete(
      'This test has not been implemented yet.'
    );
  }

}
