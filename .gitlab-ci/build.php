<?php

$this->composerRequire()
 ->dependency('drupal/webform', '^5.9')
 ->dependency('drupal/entity_print', '^2.1')
 ->dependency('phpseclib/phpseclib', '^2.0')
 ->run();
